﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using CommonProcessLibrary.TO;
using CommonProcessLibrary;
using CommonProcessLibrary.Utils;
using CommonProcessLibrary.DataAccess;
using System.Globalization;
using System.Configuration;
using Axede.Xynthesis.Log;
using CommonProcessLibrary.FTPProcess;
using System.IO;
using System.Text.RegularExpressions;

namespace Axede.Xynthesis.IpcProcess
{
    public class IpcProcess
    {
        private LogError lg = new LogError();
        //Axede.Xynthesis.IpcProcess.IpcProcess pro = new IpcProcess.IpcProcess();
        CommonProcessLibrary.DataAccess.MySQLConnection oMysqlDB = new CommonProcessLibrary.DataAccess.MySQLConnection(System.Configuration.ConfigurationSettings.AppSettings["URLDataBaseXynthesis"].ToString());
        Log.LogError Log = new LogError();
        string nroCampos = System.Configuration.ConfigurationSettings.AppSettings["nroCampos"].ToString();
        string rutaArcplano = System.Configuration.ConfigurationSettings.AppSettings["ruta_ipc_csv"].ToString();
        string nombreCampos = System.Configuration.ConfigurationSettings.AppSettings["nombreCampos"].ToString();
        string nombre_ipc_csv = System.Configuration.ConfigurationSettings.AppSettings["nombre_ipc_csv"].ToString();
        public string OpcionCargue;
        public IConnection connXynthesis;
        public Logger logger = Logger.GetInstance();
        //CommonProcessLibrary.DataAccess.MySQLConnection oMysqlDB;

        public IpcProcess()
        { }

        public string GetStartDateTickets()
        {
            string sStartDatetickets = null;
            try
            {
                connXynthesis = new MySQLConnection(ConfigurationSettings.AppSettings["URLDataBaseXynthesis"]);
                connXynthesis.OpenConnection();
                MySqlDataReader msdrQueryResult = connXynthesis.ExecuteQuery("xyp_SelMaxDateTickets", new object[] {
                    new MySqlParameter("OpcionCargue", "NORMAL")
                });
                if (msdrQueryResult.Read())
                {
                    if (!msdrQueryResult.IsDBNull(msdrQueryResult.GetOrdinal("StartDateTime")))
                        sStartDatetickets = Convert.ToString(msdrQueryResult.GetString("StartDateTime"));
                }
                msdrQueryResult.Dispose();
                msdrQueryResult = null;
                connXynthesis.CloseConnection();

                return sStartDatetickets;
            }
            catch (Exception eException)
            {

                logger.Error("Error en carga de tickets Ipc: " + eException.Message, eException);
                throw new Exception("Error capturando la última fecha de carga de tickets:\n" + "xyp_SelMaxDateTickets"
                    + "\n\nDetalle del error:\n" + eException.Message);

            }
        }

        public void Execute()
        {
            try
            {

                ExecuteETL("NORMAL");
                oMysqlDB.OpenConnection();
                oMysqlDB.ExecuteOperation("call xyp_Execute_ETL_Ipc()");
                oMysqlDB.CloseConnection();

            }
            catch (Exception eException)
            {
                logger.Error("Error en carga de tickets Ipc: " + eException.Message, eException);
            }
        }

        public void ExecuteETL(String OpcionCargue_)
        {
            OpcionCargue = OpcionCargue_;
            try
            {
                oMysqlDB.OpenConnection();
                oMysqlDB.ExecuteQuery("xyp_delCommunicationhistory");
                oMysqlDB.CloseConnection();
                extracInfoCsv();
            }
            catch (Exception ex)
            {
                Log.EscribaLog("ServicioIpc", "Error en metodo ExecuteETL : " + ex.Message, "Administrador");
                throw ex;
            }
        }

        public void transforCalls()
        {
            try
            {
                oMysqlDB.OpenConnection();
                oMysqlDB.ExecuteQuery("xyp_ExtractionTicketsIPC"); //transfroacion hacia xy_ticketinfo
                oMysqlDB.CloseConnection();

            }
            catch (Exception ex)
            {
                Log.EscribaLog("ServicioIpc", "Error en metodo transforCalls : " + ex.Message, "Administrador");
                throw ex;
            }
        }

        public void actualiUsuarios()
        {
            try
            {
                oMysqlDB.OpenConnection();
                oMysqlDB.ExecuteQuery("xyp_Execute_ETL_Ipc");  //validando usuarios en el sistema, 
                oMysqlDB.CloseConnection();

            }
            catch (Exception ex)
            {
                Log.EscribaLog("ServicioIpc", "Error en metodo actualiUsuarios : " + ex.Message, "Administrador");
                throw ex;
            }
        }


        public StreamReader extracInfoCsv_(string ftp_, string usuario, string clave)
        {
            try
            {
                FtpIPC ftp = new FtpIPC();
                return ftp.descargaArchivoFtp(ftp_, usuario, clave);
            }
            catch (Exception ex)
            {
                Log.EscribaLog("ServicioIpc", "Error en metodo extracInfoCsv : " + ex.Message, "Administrador");
                throw ex;
            }
        }

        public void extracInfoCsv()  //Extraccion de la informacion del archivo CSV
        {
            Boolean esEncabezado = false;
            string cadSql = "";
            int linea = 0, lineEncabezado = 0;
            string idLlamada = "";
            string rutaCompleta = rutaArcplano + "/" + nombre_ipc_csv;
            double duracion = 0;
            DateTime EndTime_;
            string StartDateTime;
            string EndDateTime;
            List<nodo> listaIpc = new List<nodo>();
            try
            {
                StreamReader reader;
                string[] ncampo = nombreCampos.Split(';');
                reader = new StreamReader(@rutaCompleta);

                using (reader)
                {

                    while (!reader.EndOfStream)
                    {

                        string[] values = reader.ReadLine().Split(';');
                        if (!esEncabezado)
                        {
                            // Validar nombres de os campos
                            int cuenta = 0;
                            for (int i = 0; i < values.Length; i++)
                            {
                                if (!values[i].ToUpper().Equals(ncampo[i].ToUpper()))
                                {
                                    esEncabezado = false;
                                }
                                else
                                {
                                    cuenta = cuenta + 1;
                                }
                            }
                            if (cuenta == values.Length)
                            {
                                esEncabezado = true;
                                lineEncabezado = linea;
                            }

                        }

                        if (esEncabezado)
                        {
                            if (values[0].IndexOf(Axede.Xynthesis.IpcProcess.IpcConstants.sIdenllamada) >= 0)
                            {
                                //linea que identifica la llamada
                                idLlamada = values[1];
                            }
                            if (values.Length == int.Parse(nroCampos) && values[0].IndexOf(Axede.Xynthesis.IpcProcess.IpcConstants.sIdenllamada) == -1 && linea > lineEncabezado && values[3].IndexOf(Axede.Xynthesis.IpcProcess.IpcConstants.sOmitirRegistro) == -1)
                            {
                                // Validacion de la data


                                try
                                {

                                    double seconds = TimeSpan.Parse(values[6]).TotalSeconds;
                                    //duracion = seconds + Convert.ToDouble(values[7]) + Convert.ToDouble(values[8]);
                                    duracion = Convert.ToDouble(values[7]) + Convert.ToDouble(values[8]);
                                    EndTime_ = Convert.ToDateTime(values[0]).AddSeconds(duracion);
                                }
                                catch
                                {
                                    duracion = 0;
                                    EndTime_ = Convert.ToDateTime(values[0]);
                                }
                                StartDateTime = Convert.ToDateTime(values[0]).ToString("yyyyMMdd HH:mm:ss");
                                EndDateTime = EndTime_.ToString("yyyyMMdd HH:mm:ss");
                                nodo n = new nodo();
                                n.id = idLlamada;
                                n.answeringPartyName = values[10];
                                n.buttonNumber = null;  
                                n.cLIName = values[1];
                                n.cLINumber = values[4];
                                n.callType = values[2];
                                n.callUsage = null;
                                n.connectionId = values[9];
                                n.destination = values[5];
                                n.deviceChannel = null;
                                n.deviceChannelType = null;
                                n.deviceIdId = null;
                                n.displayInCallHistory = null;
                                n.duration = TimeSpan.Parse(values[6]).TotalSeconds.ToString();
                                n.e164Destination = null;
                                n.enterpriseCallId = null;
                                n.eventType = values[3];
                                n.personalPointOfContactId = null;
                                n.pointOfContactId = null;
                                n.priority = null;
                                n.pttDuration = values[8];
                                n.reasonForDisconnect = null;
                                n.resourceAORId = null;
                                n.rolloverAppearance = null;
                                n.ringTime = values[7];
                                n.rolloverAppearance = null;
                                n.routedDestination = null;
                                n.schemaDifference_blob_reserved = null;
                                n.schemaDifference_reserved = null;
                                n.startTime = StartDateTime;
                                n.trunkId = null;
                                n.trunkBChannel = null;
                                n.userId = null;
                                n.lastModified = null;
                                n.parentUserCDIId = null;
                                n.EndTime = EndTime_.ToString("yyyyMMdd HH:mm:ss");
                                n.EffectiveCallDuration = duracion.ToString();
                                listaIpc.Add(n);
                               
                            }
                            else
                            {
                                // el numero de campos no coincide
                                //Log.EscribaLog("ConnectToDBIpc", "Error en metodo extracInfoCsv, el numero de campos exigido es : " +nroCampos+", y se reportan "+ values.Length .ToString()+ "  \n   " , "Administrador");
                                // break;
                            }

                        }
                        linea = linea + 1;
                    }  ///while

                }
                reader.Close();
                string ErrorMensage = "";
                // session de validaacion data del archivo.
                for (int j = 0; j < listaIpc.Count; j++)
                {
                    DateTime fec  ;
                    if (!DateTime.TryParse(listaIpc[j].startTime, out fec))
                        ErrorMensage = ErrorMensage + " Error en el tipo de datos fecha llamada inicial "+ listaIpc[j].startTime+" en fila "+j.ToString()+"\n";
                    
                    if (!DateTime.TryParse(listaIpc[j].EndTime, out fec))
                        ErrorMensage = ErrorMensage + " Error en el tipo de datos fecha llamada final" + listaIpc[j].EndTime + " en fila " + j.ToString() + "\n";

                    double num;
                    if (!double.TryParse(listaIpc[j].cLINumber, out num))
                        ErrorMensage = ErrorMensage + " Error en el tipo de datos telefono origen " + listaIpc[j].cLINumber + " en fila " + j.ToString() + "\n";

                    if (!double.TryParse(listaIpc[j].destination, out num))
                        ErrorMensage = ErrorMensage + " Error en el tipo de datos telefono destino" + listaIpc[j].destination + " en fila " + j.ToString() + "\n";

                    if (esHoraValida(listaIpc[j].ringTime))
                        ErrorMensage = ErrorMensage + " Error en el tipo de datos telefono ringtime" + listaIpc[j].ringTime + " en fila " + j.ToString() + "\n";

                    if (!double.TryParse(listaIpc[j].connectionId, out num))
                        ErrorMensage = ErrorMensage + " Error en el tipo de datos connectionId " + listaIpc[j].connectionId + " en fila " + j.ToString() + "\n";

                }
                if (ErrorMensage.Equals(""))
                {
                    //cargar desde la lista a la base tabla temporal xy_ipc_communicationhistory
                    for (int v=0; v<listaIpc.Count; v++)
                    {
                        oMysqlDB.OpenConnection();
                        List<object> param = new List<object>();
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("id_", listaIpc[v].id));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("answeringPartyName_", listaIpc[v].answeringPartyName));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("buttonNumber_", null));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("cLIName_", listaIpc[v].cLIName));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("cLINumber_", listaIpc[v].cLINumber));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("callType_", listaIpc[v].callType));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("callUsage_", null));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("connectionId_", listaIpc[v].connectionId));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("destination_", listaIpc[v].destination));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("deviceChannel_", null));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("deviceChannelType_", null));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("deviceIdId_", null));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("displayInCallHistory_", null));
                        //param.Add(new MySql.Data.MySqlClient.MySqlParameter("duration_", TimeSpan.Parse(values[6]).TotalSeconds.ToString()));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("duration_", listaIpc[v].duration));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("e164Destination_", null));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("enterpriseCallId_", null));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("eventType_", listaIpc[v].eventType));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("personalPointOfContactId_", null));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("pointOfContactId_", null));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("priority_", null));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("pttDuration_", listaIpc[v].pttDuration));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("reasonForDisconnect_", null));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("resourceAORId_", null));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("ringTime_", listaIpc[v].ringTime));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("rolloverAppearance_", null));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("routedDestination_", null));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("schemaDifference_blob_reserved_", null));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("schemaDifference_reserved_", null));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("startTime_", listaIpc[v].startTime));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("trunkId_", null));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("trunkBChannel_", null));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("userId_", null));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("lastModified_", null));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("parentUserCDIId_", null));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("EndTime_", listaIpc[v].EndTime));
                        param.Add(new MySql.Data.MySqlClient.MySqlParameter("EffectiveCallDuration_", listaIpc[v].duration));

                        System.Data.CommandType cmType = System.Data.CommandType.StoredProcedure;
                        oMysqlDB.ExecuteOperationByConnectedMethod("xyp_inscommunicationhistory", cmType, param);
                        oMysqlDB.CloseConnection();
                    }
                    transforCalls();
                    lg.EscribaLog("ServicioIpc", "ServicioIpc; termino el proceso con exito", "Administrador");
                }
                else
                {
                    lg.EscribaLog("ServicioIpc", "ServicioIpc; fallido el proceso de cargue con errores :: "+ ErrorMensage, "Administrador");
                }

            }
            catch (Exception ex)
            {
                Log.EscribaLog("ServicioIpc", "Error en metodo extracInfoCsv :" + cadSql + " proceso linea " + linea.ToString() + "\n" + ex.Message, "Administrador");
                throw ex;
            }

        }

        public Boolean esHoraValida(string hora)
        {
            string pattern = "\\d{1,2}:\\d{2}:\\d{2}";
            if (hora != null)
            {
                if (!Regex.IsMatch(hora, pattern, RegexOptions.CultureInvariant))
                {
                    return false;
                }
                else
                {
                    if (double.Parse(hora.Substring(0, 2)) < 0 || double.Parse(hora.Substring(0, 2)) > 24)
                    {
                        return false;
                    }
                    else
                    {
                        if (double.Parse(hora.Substring(3, 2)) < 0 || double.Parse(hora.Substring(3, 2)) > 59)
                        {
                            return false;
                        }
                        else
                        {
                            if (double.Parse(hora.Substring(6, 2)) < 0 || double.Parse(hora.Substring(6, 2)) > 59)
                            {
                                return false;
                            }
                        }
                    }
                }
            }
            return true;
        }


    }
}
